<script>
document.addEventListener('DOMContentLoaded', function () {
    const infoButtons = document.querySelectorAll('.info-btn');

    infoButtons.forEach(button => {
        button.addEventListener('click', function () {
            const infoId = this.getAttribute('data-info');
            const infoElement = document.getElementById(infoId);
            
            if (infoElement.style.display === 'block') {
                infoElement.style.display = 'none';
            } else {
                infoElement.style.display = 'block';
            }
        });
    });
});
</script>